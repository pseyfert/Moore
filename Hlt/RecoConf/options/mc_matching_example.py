###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore import options
from PyConf.application import configure_input, configure
from PyConf.control_flow import CompositeNode, NodeLogic
from RecoConf.hlt1_tracking import (
    require_gec,
    require_pvs,
    make_pvs,
    make_hlt1_tracks,
)
from RecoConf.mc_checking import monitor_tracking_efficiency, make_links_lhcbids_mcparticles_tracking_system, make_links_tracks_mcparticles
from RecoConf.mc_checking_categories import get_mc_categories, get_hit_type_mask


def mc_matching_line():
    track_type = "Forward"
    tracks = make_hlt1_tracks()[track_type]
    links_to_lhcbids = make_links_lhcbids_mcparticles_tracking_system()
    links_to_tracks = make_links_tracks_mcparticles(
        InputTracks=tracks, LinksToLHCbIDs=links_to_lhcbids)
    pr_checker = monitor_tracking_efficiency(
        TrackType=track_type,
        InputTracks=tracks,
        LinksToTracks=links_to_tracks,
        LinksToLHCbIDs=links_to_lhcbids,
        MCCategories=get_mc_categories(track_type),
        HitTypesToCheck=get_hit_type_mask(track_type),
    )

    return CompositeNode(
        'lazy_and',
        children=[require_gec(), pr_checker],
        combineLogic=NodeLogic.LAZY_AND,
        forceOrder=True)


options.histo_file = "MCMatching_Hlt1ForwardTracking.root"
config = configure_input(options)
top_cf_node = mc_matching_line()
config.update(configure(options, top_cf_node))
