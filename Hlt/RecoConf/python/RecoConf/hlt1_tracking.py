###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from PyConf import configurable
from PyConf.components import (
    make_algorithm,
    Algorithm,
)
from PyConf.application import default_raw_event

from Configurables import (
    VoidFilter,
    PrGECFilter,
    PrStoreUTHit,
    PrPixelTracking,
    PatPV3DFuture,
    PrForwardTracking,
)

from PyConf.Algorithms import (
    MakeSelection__Track_v1 as TrackV1Selection,
    MakeSelection__Track_v2 as TrackV2Selection,
    LHCb__Converters__Track__v1__fromV2TrackV1TrackVector as
    FromV2TrackV1TrackVector,
    LHCb__Converters__Track__v1__fromV2TrackV1Track as FromV2TrackV1Track,
    MakeZipContainer__Track_v2,
    FTRawBankDecoder,
    TracksVPMergerConverter,
    TracksVPMergerConverter_Clusters,
    TracksUTConverter,
    TracksFTConverter,
    TracksFitConverter,
    VPClus,
    VPClusFull,
    VeloClusterTrackingSIMD,
    TrackBeamLineVertexFinderSoA,
    PrVeloUT,
    SciFiTrackForwardingStoreHit,
    SciFiTrackForwarding,
    PrStoreFTHit,
    VeloKalman,
    TrackEventFitter,
    MakePVRelations__PrFittedForwardTracks,
    MakeZip__PrFittedForwardTracks__BestVertexRelations,
)

from PyConf.Tools import (
    PrAddUTHitsTool,
    TrackMasterExtrapolator,
    SimplifiedMaterialLocator,
    TrackMasterFitter,
    MeasurementProvider,
    MeasurementProviderT_MeasurementProviderTypes__VP_ as
    VPMeasurementProvider,
    MeasurementProviderT_MeasurementProviderTypes__UTLite_ as
    UTMeasurementProvider,
    FTMeasurementProvider,
)

from Functors import SIZE


@configurable
def default_ft_decoding_version(value=4):
    """Sets the FTDecoding version.

       Args:
           value (int): FTDecoding version.

       Returns:
           int: value
    """
    return value


UTDecoding = make_algorithm(
    PrStoreUTHit, defaults=dict(skipBanksWithErrors=True))

PixelTracking = make_algorithm(
    PrPixelTracking,
    defaults=dict(
        AlgoConfig="ForwardThenBackward",
        BoostPhysics=True,
        UsePhiPerRegionsForward=True))

PV3D = make_algorithm(
    PatPV3DFuture,
    defaults=dict(BeamSpotRCut=0.6, UseBeamSpotRCut=True, minClusterMult=4))

FastForward = make_algorithm(
    PrForwardTracking,
    defaults=dict(
        UseMomentumEstimate=True,
        MaxXGap=1.0,
        MaxXWindow=1.0,
        MinPT=400.0,
        Preselection=True,
        PreselectionPT=300.0,
        TolYCollectX=2.1875,
        TolYSlopeCollectX=0.000625))


def __emptyfilter_input_transform(InputLocation):
    return {"Cut": SIZE(InputLocation) > 0}


EmptyFilter = make_algorithm(
    VoidFilter, input_transform=__emptyfilter_input_transform)


@configurable
def require_gec(make_raw=default_raw_event, **kwargs):
    cut = 9750 if default_ft_decoding_version() == 4 else 11000
    props = dict(NumberFTUTClusters=cut, **kwargs)  # kwargs take precedence
    # PrGECFilter needs a raw event with both FT and UT raw banks
    raw = make_raw(["FTCluster", "UT"])
    return Algorithm(PrGECFilter, RawEventLocations=raw, **props)


@configurable
def make_VeloClusterTrackingSIMD_hits(make_raw=default_raw_event):
    """Makes velo hits with VeloClusterTrackingSIMD

       Args:
           make_raw (DataHandle): RawEventLocation for VeloClusterTrackingSIMD, defaults to `default_raw_event <PyConf.application.default_raw_event>`.

       Returns:
           DataHandle: VeloClusterTrackingSIMD's HitsLocation.
    """
    return VeloClusterTrackingSIMD(RawEventLocation=make_raw()).HitsLocation


def make_VPClus_location_and_offsets(make_raw=default_raw_event):
    """Makes velo clusters with VPClus

       Args:
           make_raw (DataHandle): RawEventLocation for VeloClusterTrackingSIMD, defaults to `default_raw_event <PyConf.application.default_raw_event>`.

       Returns:
           A dict mapping VPClus' ClusterLocation and ClusterOffsets DataHandles to ``'Location'`` and ``'Offsets'`` respectively.
    """
    clustering = VPClus(RawEventLocation=make_raw())
    return {
        "Location": clustering.ClusterLocation,
        "Offsets": clustering.ClusterOffsets
    }


def make_VPClus_hits():
    return make_VPClus_location_and_offsets()["Location"]


@configurable
def make_velo_full_clusters(make_raw=default_raw_event):
    return VPClusFull(RawEventLocation=make_raw()).ClusterLocation


@configurable
def make_VeloClusterTrackingSIMD_tracks(make_raw=default_raw_event):
    """Makes velo tracks with VeloClusterTrackingSIMD.

       Args:
           make_raw (DataHandle): RawEventLocation for VeloClusterTrackingSIMD, defaults to `default_raw_event <PyConf.application.default_raw_event>`.

       Returns:
           A dict mapping forward-, backward-going and v2 velo tracks to ``'Pr'``, ``'Pr::backward'`` and ``'v2'`` respectively.
    """
    tracking = VeloClusterTrackingSIMD(RawEventLocation=make_raw())
    forward_going_tracks = tracking.TracksLocation
    backward_going_tracks = tracking.TracksBackwardLocation
    v2_tracks = TracksVPMergerConverter(
        TracksForwardLocation=forward_going_tracks,
        TracksBackwardLocation=backward_going_tracks,
        HitsLocation=tracking.HitsLocation).OutputTracksLocation
    return {
        "Pr": forward_going_tracks,
        "Pr::backward": backward_going_tracks,
        "v2": v2_tracks
    }


@configurable
def make_PrPixelTracking_tracks(
        make_raw=default_raw_event,
        make_velo_clusters=make_VPClus_location_and_offsets):
    """Makes velo tracks with PrPixelTracking.

       Args:
           make_raw (DataHandle): RawEventLocation for VeloClusterTrackingSIMD, defaults to `default_raw_event <PyConf.application.default_raw_event>`.
           make_velo_clusters (DataHandle): velo custering algorithm, defaults to `make_VPClus_location_and_offsets`.

       Returns:
           A dict mapping forward-, backward-going and v2 velo tracks to ``'Pr'``, ``'Pr::backward'`` and ``'v2'`` respectively.

       Note:
           PrPixelTracking's defaults have been overridden with ``AlgoConfig="ForwardThenBackward", BoostPhysics=True, UsePhiPerRegionsForward=True``.
       """
    clusters = make_velo_clusters(make_raw)
    tracking = PixelTracking(
        ClusterLocation=clusters["Location"],
        ClusterOffsets=clusters["Offsets"])
    forward_going_tracks = tracking.OutputTracksName
    backward_going_tracks = tracking.TracksBackwardLocation
    v2_tracks = TracksVPMergerConverter_Clusters(
        TracksForwardLocation=forward_going_tracks,
        TracksBackwardLocation=backward_going_tracks,
        HitsLocation=clusters["Location"]).OutputTracksLocation
    return {
        "Pr": forward_going_tracks,
        "Pr::backward": backward_going_tracks,
        "v2": v2_tracks
    }


@configurable
def all_velo_track_types(make_velo_tracks=make_VeloClusterTrackingSIMD_tracks):
    """Helper function to get all types of velo tracks.

       Args:
           make_velo_tracks (DataHandle): velo tracking algorithm, defaults to `make_VeloClusterTrackingSIMD_tracks`.

       Returns:
           A dict mapping forward-, backward-going, v1 and v2 velo tracks to ``'Pr'``, ``'Pr::backward'``, ``'v1'`` and ``'v2'`` respectively.
    """
    velo_tracks = make_velo_tracks()
    velo_tracks_v2 = velo_tracks["v2"]
    velo_tracks_v1 = FromV2TrackV1Track(
        InputTracksName=velo_tracks_v2).OutputTracksName
    return {
        "Pr": velo_tracks["Pr"],
        "Pr::backward": velo_tracks["Pr::backward"],
        "v2": velo_tracks_v2,
        "v1": velo_tracks_v1
    }


def make_TrackBeamLineVertexFinderSoA_pvs(velo_tracks):
    """Makes primary vertices from velo tracks using TrackBeamLineVertexFinderSoA.

       Args:
           velo_tracks (dict): velo tracks, needs ``'Pr'`` and ``'Pr::backward'`` tracks, e.g. from `all_velo_track_types`.

       Returns:
           DataHandle: TrackBeamLineVertexFinderSoA's OutputVertices.
    """
    return TrackBeamLineVertexFinderSoA(
        TracksLocation=velo_tracks["Pr"],
        TracksBackwardLocation=velo_tracks["Pr::backward"]).OutputVertices


def make_PatPV3DFuture_pvs(velo_tracks):
    """Makes primary vertices from velo tracks using PatPV3DFuture.

       Args:
           velo_tracks (dict): velo tracks, needs ``'v2'`` tracks, e.g. from `all_velo_track_types`.

       Returns:
           DataHandle: PatPV3DFuture's OutputVerticesName

       Note:
           PatPV3DFuture's defaults have been overridden in this maker with ``BeamSpotRCut=0.6, UseBeamSpotRCut=True, minClusterMult=4``.
    """
    return PV3D(InputTracks=velo_tracks["v2"]).OutputVerticesName


@configurable
def make_reco_pvs(
        velo_tracks,
        make_pvs_from_velo_tracks=make_TrackBeamLineVertexFinderSoA_pvs):
    """Makes PVs from velo tracks given a PV algorithm.

       Args:
           velo_tracks (dict): velo tracks, e.g. from `all_velo_track_types`.
           make_pvs_from_velo_tracks (DataHandle): PV maker consuming ``velo_tracks``, defaults to `make_TrackBeamLineVertexFinderSoA_pvs`.

       Returns:
           DataHandle: Output of ``make_pvs_from_velo_tracks``
    """
    return make_pvs_from_velo_tracks(velo_tracks)


def make_pvs():
    """Makes PVs from HLT1 inputs, i.e. from `all_velo_track_types` using the PV maker passed to `make_reco_pvs`

       Returns:
           DataHandle: primary vertices
    """
    return make_reco_pvs(all_velo_track_types())


def require_pvs(pvs):
    """Requires that at least 1 PV was reconstructed in this event.

       Args:
           pvs (DataHandle): PV maker.

       Returns:
           VoidFilter acting on ``pvs``
    """
    return EmptyFilter(name='require_pvs', InputLocation=pvs)


@configurable
def make_PrStoreUTHit_hits(make_raw=default_raw_event):
    """Decodes UT hits from raw data.

       Args:
            make_raw (DataHandle): RawEventLocation for VeloClusterTrackingSIMD, defaults to `default_raw_event <PyConf.application.default_raw_event>`.

       Returns:
            DataHandle: PrStoreUTHit's UTHitsLocation.

       Note:
           PrStoreUTHit's defaults have been overridden in this maker with ``skipBanksWithErrors=True``.
    """
    return UTDecoding(RawEventLocations=make_raw()).UTHitsLocation


@configurable
def make_PrVeloUT_tracks(velo_tracks, make_ut_hits=make_PrStoreUTHit_hits):
    """Makes upstream tracks from velo tracks and UT hits.

       Args:
           velo_tracks (dict): velo tracks, needs ``'Pr'`` tracks, e.g. from `all_velo_track_types`.
           make_ut_hits (DataHandle): UT hit maker, defaults to `make_PrStoreUTHit_hits`.

       Returns:
           DataHandle: PrVeloUT's OutputTracksName.
    """
    return PrVeloUT(
        InputTracksName=velo_tracks["Pr"],
        UTHits=make_ut_hits()).OutputTracksName


@configurable
def all_upstream_track_types(velo_tracks,
                             make_velo_ut_tracks=make_PrVeloUT_tracks):
    """Helper function to get all types of upstream tracks.

       Args:
           velo_tracks (dict): velo tracks, needs ``'v2'`` tracks, e.g. from `all_velo_track_types`.
           make_velo_ut_tracks (DataHandle): upstream track maker, defaults to `make_PrVeloUT_tracks`.

       Returns:
           A dict mapping Pr, v1 and v2 upstream tracks to ``'Pr'``, ``'v1'`` and ``'v2'`` respectively.
    """
    upstream_tracks_pr = make_velo_ut_tracks(velo_tracks=velo_tracks)

    upstream_tracks_v2 = TracksUTConverter(
        TracksVPLocation=velo_tracks["v2"],
        TracksUTLocation=upstream_tracks_pr).OutputTracksLocation

    upstream_tracks_v1 = FromV2TrackV1Track(
        InputTracksName=upstream_tracks_v2).OutputTracksName

    return {
        "Pr": upstream_tracks_pr,
        "v2": upstream_tracks_v2,
        "v1": upstream_tracks_v1
    }


def make_FTRawBankDecoder_clusters(make_raw=default_raw_event):
    """Decodes the FT raw bank into FTLiteClusters. DecodingVersion set by `default_ft_decoding_version`.

       Args:
           make_raw (DataHandle): RawEventLocation for VeloClusterTrackingSIMD, defaults to `default_raw_event <PyConf.application.default_raw_event>`.

       Returns:
           DataHandle: FTRawBankDecoder's OutputLocation.
    """
    return FTRawBankDecoder(
        RawEventLocations=make_raw(),
        DecodingVersion=default_ft_decoding_version()).OutputLocation


@configurable
def make_SciFiTrackForwardingStoreHit_hits(
        make_ft_clusters=make_FTRawBankDecoder_clusters):
    """Transforms FTLiteClusters into the input format needed by the SciFiTrackForwarding.

       Args:
            make_ft_clusters (DataHandle): maker of FT clusters, defaults to `make_FTRawBankDecoder_clusters`.

       Returns:
           DataHandle: SciFiTrackForwardingStoreHit's Output.
    """
    return SciFiTrackForwardingStoreHit(HitsLocation=make_ft_clusters()).Output


@configurable
def make_SciFiTrackForwarding_tracks(
        input_tracks, make_ft_hits=make_SciFiTrackForwardingStoreHit_hits):
    """Makes forward tracks with SciFiTrackForwarding (long tracks from upstream seeds).

       Args:
           input_tracks (dict): upstream tracks, needs ``'Pr'`` tracks, e.g. from  `all_upstream_track_types`.
           make_ft_hits (DataHandle): maker of FT hits, defaults to `make_SciFiTrackForwardingStoreHit_hits`.

       Returns:
           DataHandle: SciFiTrackForwarding's Output.
    """
    return SciFiTrackForwarding(
        HitsLocation=make_ft_hits(), InputTracks=input_tracks["Pr"]).Output


@configurable
def get_global_ut_hits_tool(ut_hits_tool=PrAddUTHitsTool,
                            make_ut_hits=make_PrStoreUTHit_hits):
    """Defines global tool for adding UT hits.

       Args:
           ut_hits_tool: tool adding UT hits, defaults to PrAddUTHitsTool.
           make_ut_hits (DataHandle): maker of UT hits, defaults to `make_PrStoreUTHit_hits`.

       Returns:
           ``ut_hits_tool`` consuming ``make_ut_hits``.
    """
    return ut_hits_tool(UTHitsLocation=make_ut_hits())


@configurable
def make_PrStoreFTHit_hits(make_ft_clusters=make_FTRawBankDecoder_clusters):
    """Makes FT hits taking the detector geometry into account.

       Args:
           make_ft_clusters (DataHandle): maker of FT clusters, defaults to `make_FTRawBankDecoder_clusters`.

       Returns:
           DataHandle: PrStoreFTHit's FTHitsLocation.
    """
    return PrStoreFTHit(InputLocation=make_ft_clusters()).FTHitsLocation


@configurable
def make_PrForwardTracking_tracks(input_tracks,
                                  make_ft_hits=make_PrStoreFTHit_hits,
                                  ut_hits_tool=get_global_ut_hits_tool):
    """Makes forward tracks with PrForwardTracking (long tracks from upstream seeds).

       Args:
           input_tracks (dict): upstream tracks, needs ``'Pr'`` tracks, e.g. from  `all_upstream_track_types`.
           make_ft_hits (DataHandle): maker of FT hits, defaults to `make_PrStoreFTHit_hits`.
           ut_hits_tool: tool providing UT hits, defaults to `get_global_ut_hits_tool`.

       Returns:
           DataHandle: PrForwardTracking's OutputName.

       Note:
           PrForwardTracking's defaults have been overridden in this maker with ``MaxXGap=1.0, MaxXWindow=1.0, MinPT=400.0, Preselection=True, PreselectionPT=300.0, TolYCollectX=3.5, TolYSlopeCollectX=0.001``
    """
    return FastForward(
        FTHitsLocation=make_ft_hits(),
        InputName=input_tracks["Pr"],
        AddUTHitsToolName=ut_hits_tool(),
    ).OutputName


@configurable
def all_hlt1_forward_track_types(
        input_tracks, make_forward_tracks=make_SciFiTrackForwarding_tracks):
    """Helper function to get all types of HLT1 forward tracks.

       Args:
           input_tracks (dict): upstream tracks, needs ``'v2'`` tracks, e.g. from `all_upstream_track_types`.
           make_forward_tracks (DataHandle): maker of forward tracks, defaults to `make_SciFiTrackForwarding_tracks`.

       Returns:
           A dict mapping Pr, v1, v2 and v2Zip HLT1 forward tracks to ``'Pr'``, ``'v1'``, ``'v2'`` and ``'v2Zip'`` respectively.
    """
    forward_tracks_pr = make_forward_tracks(input_tracks=input_tracks)
    forward_tracks_v2 = TracksFTConverter(
        TracksUTLocation=input_tracks["v2"],
        TracksFTLocation=forward_tracks_pr).OutputTracksLocation
    forward_tracks_v1 = FromV2TrackV1Track(
        InputTracksName=forward_tracks_v2).OutputTracksName
    forward_tracks_v2_zip = MakeZipContainer__Track_v2(
        Input=forward_tracks_v2).OutputSelection
    return {
        "Pr": forward_tracks_pr,
        "v2": forward_tracks_v2,
        "v1": forward_tracks_v1,
        "v2Zip": forward_tracks_v2_zip,
    }


def make_hlt1_tracks():
    """Function to get all types of tracks reconstructed in HLT1

       Returns:
           A dict mapping all types of velo, upstream and HLT1 forward tracks to ``'Velo'``, ``'Upstream'`` and ``'Forward'`` respectively.
    """
    velo_tracks = all_velo_track_types()
    velo_ut_tracks = all_upstream_track_types(velo_tracks)
    forward_tracks = all_hlt1_forward_track_types(velo_ut_tracks)
    return {
        "Velo": velo_tracks,
        "Upstream": velo_ut_tracks,
        "Forward": forward_tracks,
    }


@configurable
def make_VeloKalman_fitted_tracks(tracks,
                                  make_hits=make_VeloClusterTrackingSIMD_hits):
    """Fits tracks with VeloKalman.

       Args:
           tracks (dict of dicts): input tracks to VeloKalman. It will consume ``["Velo"]["Pr"]`` and ``["Forward"]["Pr"]`` tracks.
           make_hits (DataHandle): maker of velo hits, defaults to `make_VeloClusterTrackingSIMD_hits`.
       Returns:
           A dict mapping Pr, v1, v2, v1Sel, v2Sel, v2Zip and v2ZipSel fitted forward tracks to ``'Pr'``, ``'v1'``, ``'v2'``, ``'v1Sel'``, ``'v2Sel'``, ``'v2Zip'`` and ``'v2ZipSel'`` respectively.
    """
    fitted_tracks = VeloKalman(
        HitsLocation=make_hits(),
        TracksVPLocation=tracks["Velo"]["Pr"],
        TracksFTLocation=tracks["Forward"]["Pr"]).OutputTracksLocation
    fitted_tracks_v2 = TracksFitConverter(
        TracksFTLocation=tracks["Forward"]["v2"],
        TracksFitLocation=fitted_tracks).OutputTracksLocation
    fitted_tracks_v1 = FromV2TrackV1Track(
        InputTracksName=fitted_tracks_v2).OutputTracksName
    fitted_tracks_v1_sel = TrackV1Selection(
        Input=FromV2TrackV1TrackVector(
            InputTracksName=fitted_tracks_v2).OutputTracksName)
    fitted_tracks_v2_sel = TrackV2Selection(Input=fitted_tracks_v2).Output
    fitted_tracks_v2_zip_sel = MakeZipContainer__Track_v2(
        Input=fitted_tracks_v2).OutputSelection
    fitted_tracks_v2_zip = MakeZipContainer__Track_v2(
        Input=fitted_tracks_v2).OutputData
    return {
        "Pr": fitted_tracks,
        "v2": fitted_tracks_v2,
        "v1": fitted_tracks_v1,
        "v1Sel": fitted_tracks_v1_sel,
        "v2Sel": fitted_tracks_v2_sel,
        "v2Zip": fitted_tracks_v2_zip,
        "v2ZipSel": fitted_tracks_v2_zip_sel,
    }


@configurable
def make_fitted_forward_tracks_with_pv_relations(tracks, make_pvs=make_pvs):
    """Zips fitted forward tracks with their best PV.

       Args:
           tracks (dict): fitted forward tracks as input to MakeZip__PrFittedForwardTracks__BestVertexRelations, needs ``["Pr"]`` tracks, e.g. from make_VeloKalman_fitted_tracks.
           make_pvs (DataHandle): maker of PVs, defaults to `make_pvs`.

       Returns:
           A dict mapping tracks zipped with PV relations to ``'PrFittedForwardWithPVs'``.
    """
    pv_container = make_pvs()
    pr_in_tracks = tracks["Pr"]  # PrFittedForwardTracks
    pv_relations = MakePVRelations__PrFittedForwardTracks(
        Input=pr_in_tracks, InputVertices=pv_container).Output
    trackrel_zip = MakeZip__PrFittedForwardTracks__BestVertexRelations(
        Input1=pr_in_tracks, Input2=pv_relations)
    return {
        "PrFittedForwardWithPVs": trackrel_zip.Output,
    }


@configurable
def get_global_materiallocator(materiallocator=SimplifiedMaterialLocator):
    """Returns instance of (Simplified/Detailed)MaterialLocator.

       Args:
           materiallocator: MaterialLocator tool, defaults to SimplifiedMaterialLocator.

       Returns:
           Instance of input tool.
    """
    return materiallocator()


@configurable
def get_global_measurement_provider(velo_hits=make_VPClus_hits,
                                    ut_hits=make_PrStoreUTHit_hits,
                                    ft_clusters=make_FTRawBankDecoder_clusters,
                                    vp_provider=VPMeasurementProvider,
                                    ut_provider=UTMeasurementProvider,
                                    ft_provider=FTMeasurementProvider):
    """Returns instance of MeasurementProvider given VP, UT and FT provider and cluster/hits.

       Args:
           velo_clusters (DataHandle): maker of velo clusters, defaults to `make_VPClus_location_and_offsets`.
           ut_clusters (DataHandle): maker of UT hits, defaults to `make_PrStoreUTHit_hits`.
           ft_clusters (DataHandle): maker of FT clusters, defaults to `make_FTRawBankDecoder_clusters`.
           vp_provider: MeasurementProvider tool for VeloPix, defaults to VPMeasurementProvider.
           ut_provider: MeasurementProvider tool for UT, defaults to UTMeasurementProvider.
           ft_provider: MeasurementProvider tool for SciFi, defaults to FTMeasurementProvider.

       Returns:
           Instance of a global MeasurementProvider.
    """
    return MeasurementProvider(
        VPProvider=vp_provider(ClusterLocation=velo_hits()),
        UTProvider=ut_provider(ClusterLocation=ut_hits()),
        FTProvider=ft_provider(ClusterLocation=ft_clusters()),
    )


@configurable
def get_track_master_fitter(
        get_materiallocator=get_global_materiallocator,
        get_measurement_provider=get_global_measurement_provider):
    """Returns instance of TrackMasterFitter given MaterialLocator and MeasurementProvider.

       Args:
           get_materiallocator: MaterialLocator tool, defaults to `get_global_materiallocator`.
           get_measurement_provider: MeasurementProvider tool, defaults to `get_global_measurement_provider`.

       Returns:
           Instance of TrackMasterFitter.
    """
    materialLocator = get_materiallocator()
    return TrackMasterFitter(
        MeasProvider=get_measurement_provider(),
        MaterialLocator=materialLocator,
        Extrapolator=TrackMasterExtrapolator(MaterialLocator=materialLocator),
    )


@configurable
def make_TrackEventFitter_fitted_tracks(tracks,
                                        fitter_tool=get_track_master_fitter):
    """Fits tracks with TrackEventFitter.

       Args:
           tracks (dict of dicts): input tracks to TrackEventFitter, needs``["Forward"]["v1"]`` tracks, e.g. from make_hlt1_tracks.
           fitter_tool: instance of track fitting tool, defaults to `get_track_master_fitter`.

       Returns:
           A dict mapping fitted HLT1 forward v1 tracks to ``'v1'``.
    """
    return {
        "v1":
        TrackEventFitter(
            TracksInContainer=tracks["Forward"]["v1"],
            Fitter=fitter_tool()).TracksOutContainer
    }


@configurable
def make_hlt1_fitted_tracks(
        tracks, make_forward_fitted_tracks=make_VeloKalman_fitted_tracks):
    """Helper function to bind to for passing forward fitted tracks.

       Args:
           tracks (dict of dicts): input tracks to the track fitter, i.e. to ``make_forward_fitted_tracks``, e.g. from make_hlt1_tracks.
           make_forward_fitted_tracks (DataHandle): track fitter, defaults to `make_VeloKalman_fitted_tracks`.

       Returns:
           A dict of fitted tracks. The content depends on the input maker
    """
    return make_forward_fitted_tracks(tracks)
