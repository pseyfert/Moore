###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from PyConf import configurable
from PyConf.Tools import (TrackStateProvider, TrackMasterExtrapolator,
                          SimplifiedMaterialLocator)

from Hlt2Conf.data_from_file import pid_algorithms, reco_unpackers


def upfront_reconstruction():
    """Return a list DataHandles that define the upfront reconstruction output.

    This differs from `reconstruction` as it should not be used as inputs to
    other algorithms, but only to define the control flow, i.e. the return
    value of this function should be ran before all HLT2 lines.

    """
    return list(reco_unpackers().values()) + list(pid_algorithms().values())


def reconstruction():
    """Return a {name: DataHandle} dict that define the reconstruction output."""
    return {k: v.OutputName for k, v in reco_unpackers().items()}


def make_protoparticles():
    return reconstruction()['ChargedProtos']


def make_pvs():
    return reconstruction()['PVs']


def stateProvider_with_simplified_geom():
    masterextrapolator = TrackMasterExtrapolator(
        MaterialLocator=SimplifiedMaterialLocator())
    return TrackStateProvider(
        public=True, Extrapolator=masterextrapolator, UnsafeClearCache=True)
