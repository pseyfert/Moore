###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import absolute_import, division, print_function
from GaudiKernel.SystemOfUnits import MeV, mm, GeV
from PyConf import configurable
from PyConf.application import make_odin
from Moore.config import HltLine
from RecoConf.hlt1_tracking import (
    require_gec,
    require_pvs,
    make_pvs,
    make_hlt1_fitted_tracks,
    EmptyFilter,
)

from RecoConf.hlt1_muonid import (make_muon_id, make_tracks_with_muon_id)

from RecoConf.hlt1_muonmatch import make_tracks_with_muonmatch_ipcut
from ..algorithms import (CombineTracks, Filter)

from Functors import ISMUON, CHI2DOF, MASS, P, PT, CHI2DOF, MINIPCUT, MINIPCHI2CUT, RUNNUMBER, EVENTTYPE, EVENTNUMBER, MINIP, BPVIPCHI2, DOCA, DOCACHI2
from Functors.math import in_range


def make_fitted_tracks_with_muon_id(velo_track_min_ip, tracking_min_pt):
    all_tracks = make_tracks_with_muonmatch_ipcut(velo_track_min_ip,
                                                  tracking_min_pt)
    # fit the reconstructed tracks to make fitted tracks
    fitted_forward_tracks = make_hlt1_fitted_tracks(all_tracks)
    # get muonIDs, this needs forward tracks (not fitted forward tracks)
    muon_ids = make_muon_id(all_tracks["Forward"])
    # bind the muonIDs and the fitted forward tracks together
    tracks_with_muon_id = make_tracks_with_muon_id(fitted_forward_tracks,
                                                   muon_ids)
    # return the type needed by the muon filter, which is the zipped object of fitted forward tracks and muonIDs
    return {'PrFittedForwardWithMuonID': tracks_with_muon_id}


def require_forward_tracks(tracks):
    return EmptyFilter(name='require_forward_tracks', InputLocation=tracks)


@configurable
def prefilters(make_pvs=make_pvs):
    return [require_gec(), require_pvs(make_pvs())]


@configurable
def detached_low_pt_muon_line(
        name='Hlt1LowPtMuonLine',
        prescale=1,
        make_input_tracks=make_fitted_tracks_with_muon_id,
        make_pvs=make_pvs,
        velo_track_min_ip=4. * mm,
        tracking_min_pt=80. * MeV,
        max_chi2dof=100.0,
        min_pt=80.0 * MeV,
        min_ipchi2=7.4):

    pvs = make_pvs().location
    sel = (ISMUON) & (PT > min_pt) & MINIPCHI2CUT(
        IPChi2Cut=min_ipchi2, Vertices=pvs) & (CHI2DOF < max_chi2dof)
    tracks_with_muon_id = make_input_tracks(velo_track_min_ip, tracking_min_pt)
    # make selection algorithm object
    trackmuon_filter = Filter(tracks_with_muon_id,
                              sel)['PrFittedForwardWithMuonID']
    return HltLine(
        name=name,
        algs=prefilters() + [trackmuon_filter],
        prescale=prescale,
    )


@configurable
def detached_low_pt_dimuon_line(
        name='Hlt1LowPtDiMuonLine',
        prescale=1,
        make_input_tracks=make_fitted_tracks_with_muon_id,
        make_pvs=make_pvs,
        velo_track_min_ip=0.1 * mm,
        tracking_min_pt=80. * MeV,
        min_p=3.0 * GeV,
        min_pt=80.0 * MeV,
        max_track_chi2dof=100.,
        min_track_ipchi2=1.,
        max_doca=0.2 * mm,
        max_vertex_chi2=25.,
        min_mass=220. * MeV):

    pvs = make_pvs().location

    tracks_with_muon_id = make_input_tracks(velo_track_min_ip, tracking_min_pt)
    # functor for the selection on the individual forward fitted tracks with muonID
    sel = (ISMUON) & (P > min_p) & (PT > min_pt) & MINIPCUT(
        IPCut=velo_track_min_ip, Vertices=pvs) & MINIPCHI2CUT(
            IPChi2Cut=min_track_ipchi2,
            Vertices=pvs) & (CHI2DOF < max_track_chi2dof)
    #get the output of the selection algortihm run over the fitted forward tracks, that will be the input tracks for the combiner
    children = Filter(tracks_with_muon_id, sel)
    # functor for the selection on the combination of 2 forward fitted tracks with muonID
    CombinationCut = (DOCA < max_doca) & (DOCACHI2 < max_vertex_chi2)
    # functor for the selection on the vertex of 2 forward fitted tracks with muonID
    VertexCut = (CHI2DOF < max_vertex_chi2) & (MASS(Masses=[105., 105.]) >
                                               min_mass)

    combination_filter = CombineTracks(
        NBodies=2,
        PrTracks=True,
        TracksWithMuonID=True,
        VertexCut=VertexCut,
        InputTracks=children['Scalar__PrFittedForwardWithMuonID'],
        CombinationCut=CombinationCut)
    return HltLine(
        name=name,
        algs=prefilters() + [combination_filter],
        prescale=prescale,
    )


@configurable
def debug_detached_low_pt_dimuon_line(
        name='Hlt1DebugLowPtDiMuonLine',
        prescale=1,
        make_input_tracks=make_fitted_tracks_with_muon_id,
        make_pvs=make_pvs,
        velo_track_min_ip=0.1 * mm,
        tracking_min_pt=80. * MeV,
        min_p=3.0 * GeV,
        min_pt=80.0 * MeV,
        max_track_chi2dof=100.,
        min_track_ipchi2=1.,
        max_doca=0.2 * mm,
        max_vertex_chi2=25.,
        min_mass=220. * MeV):

    pvs = make_pvs().location
    odin_loc = make_odin().location

    tracks_with_muon_id = make_input_tracks(velo_track_min_ip, tracking_min_pt)
    # functor for the selection on the individual forward fitted tracks with muonID
    sel = (ISMUON) & (P > min_p) & (PT > min_pt) & MINIPCUT(
        IPCut=velo_track_min_ip, Vertices=pvs) & MINIPCHI2CUT(
            IPChi2Cut=min_track_ipchi2,
            Vertices=pvs) & (CHI2DOF < max_track_chi2dof)
    #get the output of the selection algortihm run over the fitted forward tracks, that will be the input tracks for the combiner
    children = Filter(tracks_with_muon_id, sel)
    # functor for the selection on the combination of 2 forward fitted tracks with muonID
    CombinationCut = (DOCA < max_doca) & (DOCACHI2 < max_vertex_chi2)
    # functor for the selection on the vertex of 2 forward fitted tracks with muonID
    VertexCut = (CHI2DOF < max_vertex_chi2) & (MASS(Masses=[105., 105.]) >
                                               min_mass)

    VoidDump = {
        'runNumber': RUNNUMBER(odin_loc),
        'eventType': EVENTTYPE(odin_loc),
        'eventNumber': EVENTNUMBER(odin_loc),
    }
    ChildDump = {
        'P': P,
        'PT': PT,
        'MINIP': MINIP(pvs),
        'IPCHI2': BPVIPCHI2(pvs),
        'CHI2DOF': CHI2DOF
    }
    CombinationDump = {'DOCA': DOCA, 'DOCACHI2': DOCACHI2}
    VertexDump = {'MASS': MASS(Masses=[105., 105.]), 'CHI2DOF': CHI2DOF}

    combination_filter = CombineTracks(
        NBodies=2,
        PrTracks=True,
        TracksWithMuonID=True,
        VertexCut=VertexCut,
        InputTracks=children['Scalar__PrFittedForwardWithMuonID'],
        CombinationCut=CombinationCut,
        VoidDump=VoidDump,
        ChildDump=ChildDump,
        CombinationDump=CombinationDump,
        VertexDump=VertexDump,
        DumpFileName='Hlt1DiMuonLine.root')

    return HltLine(
        name=name,
        algs=prefilters() + [combination_filter],
        prescale=prescale,
    )
