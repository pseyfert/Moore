<?xml version="1.0" encoding="UTF-8"?><!DOCTYPE extension  PUBLIC '-//QM/2.3/Extension//EN'  'http://www.codesourcery.com/qm/dtds/2.3/-//qm/2.3/extension//en.dtd'>
<!--
    (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration

    This software is distributed under the terms of the GNU General Public
    Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".

    In applying this licence, CERN does not waive the privileges and immunities
    granted to it by virtue of its status as an Intergovernmental Organization
    or submit itself to any jurisdiction.
-->
<!--
Make sure all standard particle makers configure and run on reconstructed data.
-->
<extension class="GaudiTest.GaudiExeTest" kind="test">
<argument name="program"><text>gaudirun.py</text></argument>
<argument name="args"><set>
  <text>$HLT2CONFROOT/options/hlt2_standard_particles.py</text>
  <text>--output=hlt2_standard_particles.opts.py</text>
  <text>--all-opt</text>
</set></argument>
<argument name="options"><text>
from Configurables import HiveDataBrokerSvc
HiveDataBrokerSvc().OutputLevel = 5
</text></argument>
<argument name="use_temp_dir"><enumeral>true</enumeral></argument>
<argument name="validator"><text>

countErrorLines({"FATAL": 0, "ERROR": 0, "WARNING": 0})

import re
dec_matches = re.findall('LAZY_AND: (make_.*) .*', stdout)
if not dec_matches:
    causes.append('no maker decisions found')

# Test basic particle makers by capturing the number of created particles and anti-particles 
table_matches = re.findall(r' \| "Nb created (?:anti-)?particles"\s+\|\s+10 \|\s+(\d+) \|.*', stdout)
if not table_matches:
    causes.append('no maker tables found')

# Count how many standard particle makers are for composites
combiner_matches = re.findall('pass mother cut', stdout)

# Currently all basic particle makers have no filters in front of them, so should see all events
# All decisions correspond to basic makers *except* those belonging to composite makers
# Subtract 1 because the `make_particles` maker is the same as the default `make_long_pions` maker,
# so the de-duplication causes one less algorithm to run
nexpected_basic_decisions = len(dec_matches) - len(combiner_matches) - 1
# Half the number of particle counts to account for charge conjugation
nexpected_basic_makers = len(table_matches)/2
if nexpected_basic_decisions != nexpected_basic_makers:
    causes.append('not all basic particle makers processed all events')

# All particle makers should create at least some objects
min_expected_particles = 0
for nparticles in table_matches:
    if not int(nparticles) &gt; min_expected_particles:
        causes.append('{} created particles is below expected ({})'.format(nparticles, min_expected_particles))
</text></argument>
</extension>
