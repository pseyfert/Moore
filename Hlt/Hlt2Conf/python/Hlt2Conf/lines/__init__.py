###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Package containing modules that define HLT2 lines.

Modules defined within this package should register the lines they want
to export in a registry called `all_lines`.

"""
from Moore.config import add_line_to_registry
from . import (
    Bs2JpsiPhi,
    D02HH,
    D02HHHH,
)

__all__ = [
    'all_lines',
    'modules',
]


def _get_all_lines(modules):
    all_lines = {}
    for module in modules:
        try:
            lines = module.all_lines
        except AttributeError:
            raise AttributeError(
                'line module {} does not define mandatory `all_lines`'.format(
                    module.__name__))
        for name, maker in lines.items():
            add_line_to_registry(all_lines, name, maker)
    return all_lines


modules = [
    Bs2JpsiPhi,
    D02HH,
    D02HHHH,
]

all_lines = _get_all_lines(modules)
