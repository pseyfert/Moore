Documenting Moore
=================

This documentation is build by SPHINX [1]_. It was decided to stick to the google-style documentation [2]_, with an example: [3]_.

Building the documentation locally
----------------------------------

When building the docs for the first time, the following lines of code should do the trick:

.. code-block:: sh

  . /cvmfs/lhcb.cern.ch/lib/LbEnv-testing.sh
  . /cvmfs/lhcbdev.cern.ch/nightlies/lhcb-head/Yesterday/setupSearchPath.sh
  $VIRTUAL_ENV/clone_venv moore_venv
  unset LBENV_SOURCED
  source moore_venv/bin/LbEnv.sh -r $MYSITEROOT
  pip install -r Moore/doc/requirements.txt
  pip install pydot
  pip install graphviz
  Moore/run env PYTHONHOME=$(pwd)/moore_venv make -C Moore/doc html


Once the new env is set up, it can be used again like this:

.. code-block:: sh

  . /cvmfs/lhcb.cern.ch/lib/LbEnv-testing.sh
  source moore_venv/bin/LbEnv.sh -r $MYSITEROOT
  Moore/run env PYTHONHOME=$(pwd)/moore_venv make -C Moore/doc html

.. [1] https://www.sphinx-doc.org/en/master/
.. [2] https://www.sphinx-doc.org/en/master/usage/extensions/napoleon.html
.. [3] https://www.sphinx-doc.org/en/master/usage/extensions/example_google.html#example-google
