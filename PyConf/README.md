PyConf
======

PyConf makes Gaudi application configuration safer, cleaner, and simpler to debug.

More details on the components PyConf provides is given in the
[`PyConf/__init__.py` file][init]. An example configuration is given in
[`options/example.py`][example].

[Moore][moore] is a complex application that uses PyConf.

[init]: python/PyConf/__init__.py
[example]: options/example.py
[moore]: https://gitlab.cern.ch/lhcb/Moore
