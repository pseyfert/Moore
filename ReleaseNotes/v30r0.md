2018-04-16 Moore v30r0
========================================

Development release for upgrade studies
----------------------------------------
Based on Gaudi v30r2, LHCb v50r0, Lbcom v30r0, Rec v30r0, Phys v30r0, Hlt v30r0
This version is released on the master branch.

- Make Moore swallow upgrade MC, !117 (@olupton, @rmatev)
  - Make `DataType = Upgrade` (the new default) setup a functional configuration.
  - Fix or remove tests as appropriate.
- Fix conflict resolution of !132, !134 (@rmatev)
- Adapt tests to muon ID modernization, !123 (@rmatev)
- Merge 2018-patches into master, !132 (@rmatev)
- Adapt L0DU decoding in L0App and Moore, !122 (@rmatev)
- Merged future into master, !116 (@sponce)
