2018-06-21 Moore v28r3
========================================

Production version for 2018, post TS1
----------------------------------------
Based on Gaudi v29r4, LHCb v44r3, Lbcom v22r0p4, Rec v23r3, Phys v25r6, Hlt v28r3.
This version is released on the 2018-patches branch.

- Fix RateTest2 to work with TCKs, !151 (@rmatev)
